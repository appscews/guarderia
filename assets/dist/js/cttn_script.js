﻿
$(function () {

    // Devuelve un booleano si es un NSS válido
    // (deben ser 11 dígitos sin otro caracter en el medio)
    function nssValido(nss) {
        const re = /^(\d{2})(\d{2})(\d{2})\d{5}$/,
              validado = nss.match(re);

        if (!validado)  // 11 dígitos y subdelegación válida?
            return false;

        const subDeleg = parseInt(validado[1], 10),
              anno = new Date().getFullYear() % 100;
        var annoAlta = parseInt(validado[2], 10),
              annoNac = parseInt(validado[3], 10);

        //Comparar años (excepto que no tenga año de nacimiento)
        if (subDeleg != 97) {
            if (annoAlta <= anno) annoAlta += 100;
            if (annoNac <= anno) annoNac += 100;
            if (annoNac > annoAlta)
                return false; // Err: se dio de alta antes de nacer!
        }

        return luhn(nss);
    }

    // Algoritmo de Luhn
    //  https://es.wikipedia.org/wiki/Algoritmo_de_Luhn
    function luhn(nss) {
        var suma = 0,
            par = false,
            digito;

        for (var i = nss.length - 1; i >= 0; i--) {
            var digito = parseInt(nss.charAt(i), 10);
            if (par)
                if ((digito *= 2) > 9)
                    digito -= 9;

            par = !par;
            suma += digito;
        }
        return (suma % 10) == 0;
    }


    //Handler para el evento cuando cambia el input
    //Elimina cualquier caracter no numérico y comprueba validez

    $("body").on("keyup", "#MainContent_txtNewNSS", function (e) {
        var nss = $("#MainContent_txtNewNSS").val().replace(/\D+/g, ""),
            resultado = document.getElementById("MainContent_txtNewNSS"),
            valido;

        if (nssValido(nss)) { // ⬅️ Acá se comprueba
            valido = "Válido";
            resultado.classList.remove("invalid");
            resultado.classList.add("ok");
        } else {
            valido = "No válido";
            resultado.classList.remove("ok");
            resultado.classList.add("invalid");
        }

        console.log("NSS: " + nss + "\nFormato: " + valido);
    });


});