/**
 * AdminLTE Demo Menu
 * ------------------
 * You should not use this file in production.
 * This file is for demo purposes only.
 */
function ShowMLeaveTable(Clock) {
    var rows = $('table#MleaveTable tbody tr');
    var ClockRow = rows.filter('.'+Clock).show();
    rows.not(ClockRow).hide();
}

function MergeCommonRows(table) {
    var firstColumnBrakes = [];
    // iterate through the columns instead of passing each column as function parameter:
    for (var i = 1; i <= table.find('th').length; i++) {
        var previous = null, cellToExtend = null, rowspan = 1;
        table.find("td:nth-child(" + i + ")").each(function (index, e) {
            var jthis = $(this), content = jthis.text();
            // check if current row "break" exist in the array. If not, then extend rowspan:
            if (previous == content && content !== "" && $.inArray(index, firstColumnBrakes) === -1) {
                // hide the row instead of remove(), so the DOM index won't "move" inside loop.
                jthis.addClass('hidden');
                cellToExtend.attr("rowspan", (rowspan = rowspan + 1));
                
            } else {
                // store row breaks only for the first column:
                if (i === 1) {
                    firstColumnBrakes.push(index);
                }
                rowspan = 1;
                previous = content;
                cellToExtend = jthis;

            }
            
        });
    }
    // now remove hidden td's (or leave them hidden if you wish):
    $('td.hidden').remove();
}

//MergeCommonRows($("#tblHighs"));

$(function () {
    'use strict'

    /**
     * Get access to plugins
     */

    $('[data-toggle="control-sidebar"]').controlSidebar()
    $('[data-toggle="push-menu"]').pushMenu()

    var $pushMenu = $('[data-toggle="push-menu"]').data('lte.pushmenu')
    var $controlSidebar = $('[data-toggle="control-sidebar"]').data('lte.controlsidebar')
    var $layout = $('body').data('lte.layout')

    /**
     * List of all the available skins
     *
     * @type Array
     */
    var mySkins = [
      'skin-blue',
      'skin-black',
      'skin-red',
      'skin-yellow',
      'skin-purple',
      'skin-green',
      'skin-blue-light',
      'skin-black-light',
      'skin-red-light',
      'skin-yellow-light',
      'skin-purple-light',
      'skin-green-light'
    ]

    /**
     * Get a prestored setting
     *
     * @param String name Name of of the setting
     * @returns String The value of the setting | null
     */
    function get(name) {
        if (typeof (Storage) !== 'undefined') {
            return localStorage.getItem(name)
        } else {
            window.alert('Please use a modern browser to properly view this template!')
        }
    }

    /**
     * Store a new settings in the browser
     *
     * @param String name Name of the setting
     * @param String val Value of the setting
     * @returns void
     */
    function store(name, val) {
        if (typeof (Storage) !== 'undefined') {
            localStorage.setItem(name, val)
        } else {
            window.alert('Please use a modern browser to properly view this template!')
        }
    }

    /**
     * Toggles layout classes
     *
     * @param String cls the layout class to toggle
     * @returns void
     */
    function changeLayout(cls) {
        $('body').toggleClass(cls)
        $layout.fixSidebar()
        if ($('body').hasClass('fixed') && cls == 'fixed') {
            $pushMenu.expandOnHover()
            $layout.activate()
        }
        $controlSidebar.fix()
    }

    /**
     * Replaces the old skin with the new skin
     * @param String cls the new skin class
     * @returns Boolean false to prevent link's default action
     */
    function changeSkin(cls) {
        $.each(mySkins, function (i) {
            $('body').removeClass(mySkins[i])
        })

        $('body').addClass(cls)
        store('skin', cls)
        return false
    }

    /**
     * Retrieve default settings and apply them to the template
     *
     * @returns void
     */
    function setup() {
        //var tmp = get('skin')
        // DEFAULT SKIN
        var tmp = 'skin-blue-light'
        if (tmp && $.inArray(tmp, mySkins))
            changeSkin(tmp)

        // Add the change skin listener
        $('[data-skin]').on('click', function (e) {
            if ($(this).hasClass('knob'))
                return
            e.preventDefault()
            changeSkin($(this).data('skin'))
        })

        // Add the layout manager
        $('[data-layout]').on('click', function () {
            changeLayout($(this).data('layout'))
        })

        $('[data-controlsidebar]').on('click', function () {
            changeLayout($(this).data('controlsidebar'))
            var slide = !$controlSidebar.options.slide

            $controlSidebar.options.slide = slide
            if (!slide)
                $('.control-sidebar').removeClass('control-sidebar-open')
        })

        $('[data-sidebarskin="toggle"]').on('click', function () {
            var $sidebar = $('.control-sidebar')
            if ($sidebar.hasClass('control-sidebar-dark')) {
                $sidebar.removeClass('control-sidebar-dark')
                $sidebar.addClass('control-sidebar-light')
            } else {
                $sidebar.removeClass('control-sidebar-light')
                $sidebar.addClass('control-sidebar-dark')
            }
        })

        $('[data-enable="expandOnHover"]').on('click', function () {
            $(this).attr('disabled', true)
            $pushMenu.expandOnHover()
            if (!$('body').hasClass('sidebar-collapse'))
                $('[data-layout="sidebar-collapse"]').click()
        })

        //  Reset options
        if ($('body').hasClass('fixed')) {
            $('[data-layout="fixed"]').attr('checked', 'checked')
        }
        if ($('body').hasClass('layout-boxed')) {
            $('[data-layout="layout-boxed"]').attr('checked', 'checked')
        }
        if ($('body').hasClass('sidebar-collapse')) {
            $('[data-layout="sidebar-collapse"]').attr('checked', 'checked')
        }

    }

    // Create the new tab
    var $tabPane = $('<div />', {
        'id': 'control-sidebar-theme-demo-options-tab',
        'class': 'tab-pane active'
    })

    // Create the tab button
    var $tabButton = $('<li />', { 'class': 'active' })
      .html('<a href=\'#control-sidebar-theme-demo-options-tab\' data-toggle=\'tab\'>'
        + '<i class="fa fa-wrench"></i>'
        + '</a>')

    // Add the tab button to the right sidebar tabs
    $('[href="#control-sidebar-home-tab"]')
      .parent()
      .before($tabButton)

    // Create the menu
    var $demoSettings = $('<div />')

    // Layout options
    $demoSettings.append(
      '<h4 class="control-sidebar-heading">'
      + 'Layout Options'
      + '</h4>'
      // Fixed layout
      + '<div class="form-group">'
      + '<label class="control-sidebar-subheading">'
      + '<input type="checkbox"data-layout="fixed"class="pull-right"/> '
      + 'Fixed layout'
      + '</label>'
      + '<p>Activate the fixed layout. You can\'t use fixed and boxed layouts together</p>'
      + '</div>'
      // Boxed layout
      + '<div class="form-group">'
      + '<label class="control-sidebar-subheading">'
      + '<input type="checkbox"data-layout="layout-boxed" class="pull-right"/> '
      + 'Boxed Layout'
      + '</label>'
      + '<p>Activate the boxed layout</p>'
      + '</div>'
      // Sidebar Toggle
      + '<div class="form-group">'
      + '<label class="control-sidebar-subheading">'
      + '<input type="checkbox"data-layout="sidebar-collapse"class="pull-right"/> '
      + 'Toggle Sidebar'
      + '</label>'
      + '<p>Toggle the left sidebar\'s state (open or collapse)</p>'
      + '</div>'
      // Sidebar mini expand on hover toggle
      + '<div class="form-group">'
      + '<label class="control-sidebar-subheading">'
      + '<input type="checkbox"data-enable="expandOnHover"class="pull-right"/> '
      + 'Sidebar Expand on Hover'
      + '</label>'
      + '<p>Let the sidebar mini expand on hover</p>'
      + '</div>'
      // Control Sidebar Toggle
      + '<div class="form-group">'
      + '<label class="control-sidebar-subheading">'
      + '<input type="checkbox"data-controlsidebar="control-sidebar-open"class="pull-right"/> '
      + 'Toggle Right Sidebar Slide'
      + '</label>'
      + '<p>Toggle between slide over content and push content effects</p>'
      + '</div>'
      // Control Sidebar Skin Toggle
      + '<div class="form-group">'
      + '<label class="control-sidebar-subheading">'
      + '<input type="checkbox"data-sidebarskin="toggle"class="pull-right"/> '
      + 'Toggle Right Sidebar Skin'
      + '</label>'
      + '<p>Toggle between dark and light skins for the right sidebar</p>'
      + '</div>'
    )
    var $skinsList = $('<ul />', { 'class': 'list-unstyled clearfix' })

    // Dark sidebar skins
    var $skinBlue =
          $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px;' })
            .append('<a href="javascript:void(0)" data-skin="skin-blue" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">'
              + '<div><span style="display:block; width: 20%; float: left; height: 7px; background: #367fa9"></span><span class="bg-light-blue" style="display:block; width: 80%; float: left; height: 7px;"></span></div>'
              + '<div><span style="display:block; width: 20%; float: left; height: 20px; background: #222d32"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>'
              + '</a>'
              + '<p class="text-center no-margin">Blue</p>')
    $skinsList.append($skinBlue)
    var $skinBlack =
          $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px;' })
            .append('<a href="javascript:void(0)" data-skin="skin-black" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">'
              + '<div style="box-shadow: 0 0 2px rgba(0,0,0,0.1)" class="clearfix"><span style="display:block; width: 20%; float: left; height: 7px; background: #fefefe"></span><span style="display:block; width: 80%; float: left; height: 7px; background: #fefefe"></span></div>'
              + '<div><span style="display:block; width: 20%; float: left; height: 20px; background: #222"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>'
              + '</a>'
              + '<p class="text-center no-margin">Black</p>')
    $skinsList.append($skinBlack)
    var $skinPurple =
          $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px;' })
            .append('<a href="javascript:void(0)" data-skin="skin-purple" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">'
              + '<div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-purple-active"></span><span class="bg-purple" style="display:block; width: 80%; float: left; height: 7px;"></span></div>'
              + '<div><span style="display:block; width: 20%; float: left; height: 20px; background: #222d32"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>'
              + '</a>'
              + '<p class="text-center no-margin">Purple</p>')
    $skinsList.append($skinPurple)
    var $skinGreen =
          $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px;' })
            .append('<a href="javascript:void(0)" data-skin="skin-green" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">'
              + '<div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-green-active"></span><span class="bg-green" style="display:block; width: 80%; float: left; height: 7px;"></span></div>'
              + '<div><span style="display:block; width: 20%; float: left; height: 20px; background: #222d32"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>'
              + '</a>'
              + '<p class="text-center no-margin">Green</p>')
    $skinsList.append($skinGreen)
    var $skinRed =
          $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px;' })
            .append('<a href="javascript:void(0)" data-skin="skin-red" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">'
              + '<div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-red-active"></span><span class="bg-red" style="display:block; width: 80%; float: left; height: 7px;"></span></div>'
              + '<div><span style="display:block; width: 20%; float: left; height: 20px; background: #222d32"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>'
              + '</a>'
              + '<p class="text-center no-margin">Red</p>')
    $skinsList.append($skinRed)
    var $skinYellow =
          $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px;' })
            .append('<a href="javascript:void(0)" data-skin="skin-yellow" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">'
              + '<div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-yellow-active"></span><span class="bg-yellow" style="display:block; width: 80%; float: left; height: 7px;"></span></div>'
              + '<div><span style="display:block; width: 20%; float: left; height: 20px; background: #222d32"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>'
              + '</a>'
              + '<p class="text-center no-margin">Yellow</p>')
    $skinsList.append($skinYellow)

    // Light sidebar skins
    var $skinBlueLight =
          $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px;' })
            .append('<a href="javascript:void(0)" data-skin="skin-blue-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">'
              + '<div><span style="display:block; width: 20%; float: left; height: 7px; background: #367fa9"></span><span class="bg-light-blue" style="display:block; width: 80%; float: left; height: 7px;"></span></div>'
              + '<div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>'
              + '</a>'
              + '<p class="text-center no-margin" style="font-size: 12px">Blue Light</p>')
    $skinsList.append($skinBlueLight)
    var $skinBlackLight =
          $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px;' })
            .append('<a href="javascript:void(0)" data-skin="skin-black-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">'
              + '<div style="box-shadow: 0 0 2px rgba(0,0,0,0.1)" class="clearfix"><span style="display:block; width: 20%; float: left; height: 7px; background: #fefefe"></span><span style="display:block; width: 80%; float: left; height: 7px; background: #fefefe"></span></div>'
              + '<div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>'
              + '</a>'
              + '<p class="text-center no-margin" style="font-size: 12px">Black Light</p>')
    $skinsList.append($skinBlackLight)
    var $skinPurpleLight =
          $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px;' })
            .append('<a href="javascript:void(0)" data-skin="skin-purple-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">'
              + '<div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-purple-active"></span><span class="bg-purple" style="display:block; width: 80%; float: left; height: 7px;"></span></div>'
              + '<div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>'
              + '</a>'
              + '<p class="text-center no-margin" style="font-size: 12px">Purple Light</p>')
    $skinsList.append($skinPurpleLight)
    var $skinGreenLight =
          $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px;' })
            .append('<a href="javascript:void(0)" data-skin="skin-green-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">'
              + '<div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-green-active"></span><span class="bg-green" style="display:block; width: 80%; float: left; height: 7px;"></span></div>'
              + '<div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>'
              + '</a>'
              + '<p class="text-center no-margin" style="font-size: 12px">Green Light</p>')
    $skinsList.append($skinGreenLight)
    var $skinRedLight =
          $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px;' })
            .append('<a href="javascript:void(0)" data-skin="skin-red-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">'
              + '<div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-red-active"></span><span class="bg-red" style="display:block; width: 80%; float: left; height: 7px;"></span></div>'
              + '<div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>'
              + '</a>'
              + '<p class="text-center no-margin" style="font-size: 12px">Red Light</p>')
    $skinsList.append($skinRedLight)
    var $skinYellowLight =
          $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px;' })
            .append('<a href="javascript:void(0)" data-skin="skin-yellow-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">'
              + '<div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-yellow-active"></span><span class="bg-yellow" style="display:block; width: 80%; float: left; height: 7px;"></span></div>'
              + '<div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>'
              + '</a>'
              + '<p class="text-center no-margin" style="font-size: 12px">Yellow Light</p>')
    $skinsList.append($skinYellowLight)

    $demoSettings.append('<h4 class="control-sidebar-heading">Skins</h4>')
    $demoSettings.append($skinsList)

    $tabPane.append($demoSettings)
    $('#control-sidebar-home-tab').after($tabPane)

    //setup()

    $('[data-toggle="tooltip"]').tooltip()


    $('#main_table').DataTable();


    window.ShowModalImage = function (path) {
        alert(path + "");
    }
    

    setTimeout(function (e) {
        $("#MainContent_txtSearch").click()
    },500)




})

$("body").on("click", ".upload-image-preview", function (e) {
    $("#btnChoose").click();
});

var counter;
function UploadFile() {
    //var files = $("#<%=file1.ClientID%>").get(0).files; MainContent_file1
    var files = $("#MainContent_file1").get(0).files;
    counter = 0;

    // Loop through files
    for (var i = 0; i < files.length ; i++) {
        var file = files[i];
        var formdata = new FormData();
        formdata.append("file1", file);
        var ajax = new XMLHttpRequest();

        ajax.upload.addEventListener("progress", progressHandler, false);
        ajax.addEventListener("load", completeHandler, false);
        ajax.addEventListener("error", errorHandler, false);
        ajax.addEventListener("abort", abortHandler, false);
        ajax.open("POST", "Upload_Images.ashx");
        ajax.send(formdata);
    }
}

function progressHandler(event) {
    $("#loaded_n_total").html("Subidos " + event.loaded + " de " + event.total+" bytes");
    var percent = (event.loaded / event.total) * 100;
    $("#progressBar").val(Math.round(percent));
    $("#status").html(Math.round(percent) + "% Completado");
}

function completeHandler(event) {
    counter++
    //$("#status").html(counter + " " + event.target.responseText);
    $("#divAlert").html("<div class=\"callout callout-success\" style=\"height:80px\"><h4>" + counter + " " + event.target.responseText+"</h4></div>");
    $("#divAlert").fadeIn("slow");
    setTimeout(function () {
        $("#divAlert").fadeOut("3000");
    }, 3000);
    
}

function errorHandler(event) {
    $("#status").html("Ocurrieron problemas durante la subida");
}

function abortHandler(event) {
    $("#status").html("Operacion Interrumpida");
}

$("#MainContent_file1").change(function () {
    $('.upload-image-preview').html("");
    var files = $("#MainContent_file1").get(0).files;
    
    // Loop through files
    for (var i = 0; i < files.length ; i++) {

        var file = files[i];
        var reader = new FileReader();

        var FileUploadPath = files[i].name;
        var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();
        if (Extension == "jpg") {
            reader.onload = function (e) {
                var img = $('<img class="img-preview col-md-1">').attr('src', e.target.result);
                $('.upload-image-preview').append(img);
            };

            reader.readAsDataURL(this.files[i]);
        }
        else {
            $("#divAlert").html("<div class=\"callout callout-warning\" style=\"height:80px\"><h4>Formato Incorrecto</h4><p>El formato de imagen debe ser JPG </p></div>");
            $("#divAlert").fadeIn("slow");
            setTimeout(function () {
                $("#divAlert").fadeOut("3000");

            }, 5000);
        }
        }

});

$("body").on("click", "#btnChoose", function (e) {
    $("#MainContent_file1").click();
});


/* END Headount Images*/

function EnterEvent(e) {
    if (e.keyCode == 13) {
        alert("")
        __doPostBack('<%=btnSearch.UniqueID%>', "");
    }
}

$(document).ready(function () {
    //
    
    setTimeout(function (e) {
        Pace.restart()
        $("#MedicalLeaveTableID").DataTable();
    },1000)
})

$(document).ajaxStart(function () {
    Pace.restart()
})



setTimeout(function () { $(".callout").fadeOut(2000) }, 2000)



/*
* CARGAR HC
*
*/
$("body").on("click", "#AnchorImportHC", function (e) {
    $("#MainContent_FileUpload1").click();
})

$("body").on("change", "#MainContent_FileUpload1", function (e) {
    var file = $(this).val();
    var filename = file.replace(/^.*[\\\/]/, '');
    if (file) {
        var ext = file.split('.').pop();
        console.log(ext)
        if (ext == "xlsx" || ext == "xls") {
            $("#MainContent_btnImport").removeClass("hidden");
        } else {
            filename = "Archivo no valido [" + ext + "]";
            $("#MainContent_btnImport").addClass("hidden");
        }
    }
    else {
        $("#MainContent_btnImport").addClass("hidden");
    }
    
    $("#lblFileNameImportHC").text(filename)
})

$("body").on("click", "#MainContent_btnImport", function (e) {
    document.getElementById("overlay").style.display = "block";
})

var date = new Date();
date.setDate(date.getDate() + 1);
date.setHours(6);
date.setMinutes(0);

$('.Datepicker').dateTimePicker({
    selectData: date,
    dateFormat: "YYYY-MM-DD HH:mm"
});




$("#MainContent_gdvAllRecruited").dataTable({
    "aaSorting": []
});
$("#tblHighs").dataTable({
    "aaSorting": []
});

$(".fordataTable").dataTable({
    "aaSorting": []
});


$(document).ajaxComplete(function () {
    Pace.restart()
})