﻿$(function () {
    $('#MainContent_lstExperienceAreas').multiselect({
        includeSelectAllOption: true,
        buttonWidth: '100%'
    });
    $('#MainContent_lstRDDocumentation').multiselect({
        includeSelectAllOption: true,
        buttonWidth: '100%'
    });
  
    $('body').on('change', '#MainContent_txtRDBirthday', function (e) {

        $('#MainContent_txtRDAge').val(CalculateAgeByBirthday());

    })

    $('body').on('keyup', '#MainContent_txtRDBirthday', function (e) {
        var x = $('#MainContent_txtRDBirthday').val().replace(/\D/g, '').match(/(\d{2})(\d{2})(\d{4})/);
        if(x != null)
            $('#MainContent_txtRDBirthday').val('' + x[3] + '-' + x[2] + '-' + x[1]);
        
        $('#MainContent_txtRDAge').val(CalculateAgeByBirthday());
    })

    function CalculateAgeByBirthday() {

        var date = $('#MainContent_txtRDBirthday').val();

        var birthday = new Date(date);

        var ageDifMs = Date.now() - birthday.getTime();
        var ageDate = new Date(ageDifMs); // miliseconds from epoch

        var Age = Math.abs(ageDate.getUTCFullYear() - 1970);

        console.log(Age);

        if(isNaN(Age)){
            Age = 0;
        }
       
        return Age;
    }
});