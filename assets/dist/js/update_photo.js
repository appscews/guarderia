﻿//var pageUrl = '<%=ResolveUrl("~/update_photos.aspx") %>';         /*cambie CS por nombre del formulario*/

var basic;
$(function () {



    jQuery("#webcam").webcam({
        width: 720,
        height: 480,
        mode: "save",
        swffile: "assets/Webcam_Plugin/jscam.swf",    /*marca error en el response*/
        debug: function (type, status) {
            $('#camStatus').append(type + ": " + status + '<br /><br />');
        },
        onSave: function (data) {

            $.ajax({
                type: "POST",
                url: "update_photos.aspx/GetCapturedImage",
                data: '',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) {
                    //$("[id*=imgCapture]").css("visibility", "visible");
                    $("[id*=imgCapture]").attr("src", r.d);

                        basic = $('#imgCrop').croppie({
                        viewport: {
                            width: 360,
                            height: 480
                        },
                        boundary: { width: 720, height: 480 },
                        showZoomer: false,
                        url: r.d
                        });

                        $("#webcam").attr("class", "hidden");
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        },
        onCapture: function () {
            webcam.save("update_photos.aspx");
        }
    });
});
function Capture() {
    webcam.capture();
    return false;
}

function Reset() {
    basic.destroy();
    return false;
}

function Save() {
    basic.croppie('result', {
        type: 'canvas',
        size: 'original',
        format: 'png',
        quality: 0.5,
    }).then(function (response) {
        $.ajax({
            url: "./queries/upload.php",
            type: "POST",
            dataType: 'json',
            data: { "image": response },
            success: function (data) {
                $('#uploadimageModal').modal('hide');
                image_placing = $('#uploaded_image').find('img');
                image_placing.each(function () {
                    if ($(this).attr('data-reading') == GetCropper) {

                        $(this).attr('src', '../images/uploads/' + encodeURI("%") + 'temp' + encodeURI("%") + '/' + data['image'] + '');
                        $(this).next().val(data['image'].split('.').slice(0, -1).join('.').concat('.' + data['filetype']));
                    }
                })

            }
        });
    })
}