﻿var HCDataTable;



$(function () {

    var delay = (function () {
        var timer = 0;
        return function (callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();

    
    $("#MainContent_gdvAllEmployee").DataTable()
    

    window.HCDataTable = $("#HeadCountTable").DataTable();

    $("body").on("keyup", "#MainContent_txtSearch$$", function (e) { //$$$$ es para deshabilitar la accion

        var Clock = $(this).val().toString().trim();
        console.log("#"+Clock+"#");
        
        delay(function () {

            if (Clock != "") {
                $("#MainContent_txtSearch").prop('disabled', true);
                var Query = "select NumReloj,Nombre,ingreso,Turno,TipoEmp,Puesto,Linea,Asistencia,Supervisor,Genero,FechaNacimiento,Depto, (Select top 1 'Incapacitado' from incapacidad I where I.numreloj = H.numreloj and '" + getTodayDate() + "'  between inicio and fin )'Incapacidad' from headcount H WHERE H.numreloj like '%" + Clock + "%' order by H.numreloj";
                var data = '{Query:"' + Query + '"}';

                $.ajax({
                    async: true,
                    type: "POST",
                    url: 'AjaxRequest.aspx/newSelectQuery',
                    data: data,
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json',
                    success: function (data) {
                        var HCArray = $.parseJSON(data.d).Table1;
                        //ArrayTotalProgram = objdata.Table1;

                        //HCDataTable.destroy();

                        HCDataTable.clear().draw();
                        var rowString = "";
                        //console.log(HCArray);
                        $("#HeadCountTable > tbody").html("");
                        HCArray.forEach(function (row, index) {
                            if (row) {

                                row = CleanArray(row);
                                //console.log(row);
                                var tdClass = " text-center ";
                                var fotoUrl = "<img src='http://192.168.144.10:82/Imagenes/" + row[0].trim() + ".jpg" + "' width='120px' class='img-thumbnail'>";
                                rowString = "<tr class=''>" +
                                //"<td class='" + tdClass + "'>" + row["Numreloj"] + "</td>"+
                                "<td class='" + tdClass + "'>" + fotoUrl + "</td>" +
                                "<td class='" + tdClass + "'><small class='label bg-black' style=\"font-size:13px;\" >" + row[0] + "</small><br/><br/>" + row[1] + "</td>" + // Nombre
                                "<td class='" + tdClass + "'>" + row[2] + "</td>" + //Ingreso
                                "<td class='" + tdClass + "'>" + row[3] + "</td>" + //Turno
                                "<td class='" + tdClass + "'>" + row[5] + "</td>" + //Puesto
                                "<td class='" + tdClass + "'>" + row[6] + "</td>" + //Linea
                                "<td class='" + tdClass + "'>" + row[11] + "</td>" + //Depto
                                "<td class='" + tdClass + "'>" + row[7] + "</td>" + //Asistencia
                                //"<td class='" + tdClass + "'>" + composeAttendance(row[7].ToString(), row[12].ToString().Trim(), row[0].ToString().Trim()) + "</td>" +
                                "<td class='" + tdClass + "'>" + row[8] + "</td>" + //Supervisor
                                //"<td class='" + tdClass + "'>" + composeGender(row[9].ToString()) + "</td>" + //Genero
                                "<td class='" + tdClass + "'>" + row[9] + "</td>" + //FechaNacimiento
                                "<td class='" + tdClass + "'>" + row[10] + "</td>" + //FechaNacimiento
                                "</tr>";
                                HCDataTable.rows.add($(rowString)).draw()
                                //console.log(rowString);
                                //$("#HeadCountTable > tbody").append(rowString);

                            }
                        })
                    },
                    error: function (error) {
                        console.error(error)
                    },
                    complete: function (e) {
                        $("#MainContent_txtSearch").prop('disabled', false);
                    }
                });
            }
        }, 1000);
        
        
        //console.log(rowString);
        
        //$("#HeadCountTable > tbody").html(rowString);
        //HCDataTable = $("#HeadCountTable").DataTable();
        //console.log(HCArray);
        //HCDataTable.destroy()
    })

    function newAjaxRequest(data, Method) {

        //var idGroup = (data);
        //console.log(idG);
        var ArrayTotalProgram = 0;
        $.ajax({
            async: false,
            type: "POST",
            url: 'AjaxRequest.aspx/' + Method,
            data: data,
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            success: function (data) {
                ArrayTotalProgram = $.parseJSON(data.d);
                //ArrayTotalProgram = objdata.Table1;
            },
            error: function (error) {
                console.error(error)
            }
        });

        return ArrayTotalProgram;
    }

    function getTodayDate() {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        return yyyy + "-"+mm+"-"+dd;
    }

    function CleanArray(arr) {
        arr.forEach(function(item,index){
            if (item) {
                item = item.toString().trim();
            }
        })
        return arr;
    }
})
