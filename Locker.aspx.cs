﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Asistencia : System.Web.UI.Page
{
    
    data sqlquery = new data();
    DataTable tabla = new DataTable();
    DataTable tabla2 = new DataTable();
    string turno = "Turno 1";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            tabla = sqlquery.get_inventario("select L.Locker, L.Numreloj, (select h.nombre from headcount H where h.numreloj=l.numreloj)'Nombre',Comentario from lockers L where estado=1  order by locker");
            GridView1.DataSource = tabla;
            GridView1.DataBind();
            tabla = sqlquery.get_inventario("select L.Locker,'Disponible' from lockers L where estado=0  order by locker");
            GridView2.DataSource = tabla;
            GridView2.DataBind();
            Borrar.Visible = false;
            Asignar.Visible = false;
            Foto.Visible = false;
            Checada.Visible = false;
        }

    }
    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void GridView1_RowCommand(object sender, CommandEventArgs e)
    {


        int index = Convert.ToInt32(e.CommandArgument);


        string llave = GridView1.Rows[index].Cells[1].Text.Trim();
        tabla = sqlquery.get_inventario("select L.Locker, L.Numreloj, (select h.nombre from headcount H where h.numreloj=l.numreloj)'Nombre',(select h.turno from headcount H where h.numreloj=l.numreloj)'Turno',(select h.depto from headcount H where h.numreloj=l.numreloj)'Departamento',(select h.linea from headcount H where h.numreloj=l.numreloj)'Linea',(select h.puesto from headcount H where h.numreloj=l.numreloj)'Puesto' from lockers L where locker=" + llave);
        GridView1.DataSource = tabla;
        GridView1.DataBind();
        tabla = sqlquery.get_inventario("");
        GridView2.DataSource = tabla;
        GridView2.DataBind();
        GridView2.Visible = false;
        Foto.Visible = Checada.Visible = Borrar.Visible = true;
        Image2.ImageUrl = "~/imagenes/" + GridView1.Rows[0].Cells[2].Text.Trim() + ".jpg";
        Label2.Visible = false;
        Image2.Height = 150;
        Image2.Height = 200;
        
        Label5.Text ="Ultima Checada "+ sqlquery.get_fechahp(" select E.numemp, E.apa_emp, E.AMA_EMP,nom_emp, E.emp_id, (Select top 1 C.fecha_chq from carga C where C.emp_id = E.emp_id order by fecha_chq desc ) from empleado E where numemp='" + GridView1.Rows[0].Cells[2].Text.Trim() + "'  order by numemp");
        TimeSpan dias;
        try
        {
            dias = DateTime.Now - DateTime.Parse(sqlquery.get_fechahp(" select E.numemp, E.apa_emp, E.AMA_EMP,nom_emp, E.emp_id, (Select top 1 C.fecha_chq from carga C where C.emp_id = E.emp_id order by fecha_chq desc ) from empleado E where numemp='" + GridView1.Rows[0].Cells[2].Text.Trim() + "'  order by numemp"));

            if (dias.TotalDays > 10)
            {
                Label5.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                Label5.ForeColor = System.Drawing.Color.Green;
            }
        }
        catch (Exception E)
        {
            Label5.ForeColor = System.Drawing.Color.Red;
        }


    }

    protected void GridView1_SelectedIndexChanged1(object sender, EventArgs e)
    {

    }

    protected void GridView2_SelectedIndexChanged1(object sender, EventArgs e)
    {

    }
    protected void GridView2_RowCommand(object sender, CommandEventArgs e)
    {
        try
        {
            int index = Convert.ToInt32(e.CommandArgument);

            Asignar.Visible = true;
            string llave = GridView2.Rows[index].Cells[1].Text.Trim();
            tabla = sqlquery.get_inventario("select L.Locker, 'Disponible' from lockers L where locker=" + llave);
            GridView2.DataSource = tabla;
            GridView2.DataBind();
            tabla = sqlquery.get_inventario("");
            GridView1.DataSource = tabla;
            GridView1.DataBind();
            GridView1.Visible = false;
            Borrar.Visible = false;
            Label1.Visible = false;
            TextBox1.Focus();
        }
        catch (Exception) { }
        
        }


    protected void Button3_Click(object sender, EventArgs e)
    {
        sqlquery.get_inventario("Update lockers set estado=0, numreloj='' where locker=" + GridView1.Rows[0].Cells[1].Text.Trim());
        sqlquery.get_inventario("insert into lockerhistorico values(" + GridView1.Rows[0].Cells[1].Text.Trim() + ",sysdatetime(),'Liberar','" + GridView1.Rows[0].Cells[2].Text.Trim() + "') ");
        Response.Redirect("Locker.aspx");
    }

    protected void Button4_Click(object sender, EventArgs e)
    {
        Response.Redirect("Locker.aspx");
    }

    protected void Button5_Click(object sender, EventArgs e)
    {
        if (sqlquery.get_inventariohp("select E.numemp, E.apa_emp, E.AMA_EMP,nom_emp, E.emp_id, (Select top 1 C.fecha_chq from carga C where C.emp_id = E.emp_id order by fecha_chq desc ) from empleado E where numemp='" + TextBox1.Text.Trim() + "'  order by numemp").Rows.Count>0)
        {
            if (sqlquery.get_inventario("select * from lockers where numreloj='" + TextBox1.Text.Trim() + "'").Rows.Count == 0)
            {
                sqlquery.get_inventario("Update lockers set estado=1,comentario='" + TextBox2.Text.Trim() + "', numreloj='" + TextBox1.Text.Trim() + "' where locker=" + GridView2.Rows[0].Cells[1].Text.Trim());
                sqlquery.get_inventario("insert into lockerhistorico values(" + GridView2.Rows[0].Cells[1].Text.Trim() + ",sysdatetime(),'Asignar','" + TextBox1.Text.Trim() + "') ");
                Response.Redirect("Locker.aspx");
            }
            else
            {
                Label4.Text = "El empleado ya cuenta con un locker asignado.";
                TextBox1.Focus();
            }
        }
        else
        {
            Label4.Text = "Numero de reloj no capturado en sistema.";
            TextBox1.Focus();
        }
        
    }

    protected void Button6_Click(object sender, EventArgs e)
    {
        Response.Redirect("Locker.aspx");
    }
}