﻿using Microsoft.CSharp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;



public class GridViewExportUtil
{
    public static void export(string filename, GridView gridview1, string macropath)
    {
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename=" + filename + ""));
        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";

        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);

        htw.Write("<html xmlns:x=\"urn:schemas-microsoft-com:office:excel\\\"><head>");
        htw.Write("<meta http-equiv=Content-Type content=\"text/html;charset=windows-1252\\\">");
        htw.Write("<meta name=ProgId content=Excel.Sheet>");
        htw.Write("<meta name=Generator content=\"Microsoft Excel 11\\\">");
        //htw.Write("<link rel=Edit-Time-Data href=""http://localhost:3327/Quality/reporteval.mso"">")
        htw.Write("<link rel=Edit-Time-Data href=\"" + macropath + "\">");



        htw.Write("</head>");
        using (sw)
        {
            using (htw)
            {
                //create table
                Table table = new Table();
                table.GridLines = gridview1.GridLines;



                if (!object.ReferenceEquals(gridview1.HeaderRow, DBNull.Value))
                {
                    GridViewExportUtil.PrepareControlForExport(gridview1.HeaderRow);
                    table.Rows.Add(gridview1.HeaderRow);
                }
                //add each data rows
                GridViewRow row = null;
                foreach (GridViewRow row_loopVariable in gridview1.Rows)
                {
                    row = row_loopVariable;
                    GridViewExportUtil.PrepareControlForExport(row);
                    table.Rows.Add(row);
                }

                if (!object.ReferenceEquals(gridview1.FooterRow, DBNull.Value))
                {
                    GridViewExportUtil.PrepareControlForExport(gridview1.FooterRow);
                    table.Rows.Add(gridview1.FooterRow);
                }

                table.RenderControl(htw);

                HttpContext.Current.Response.Write(sw.ToString());
                HttpContext.Current.Response.End();
            }
        }
    }

    private static void PrepareControlForExport(Control control)
    {
        int i = 0;
        while ((i < control.Controls.Count))
        {
            Control current = control.Controls[i];


            if (current.HasControls())
            {
                GridViewExportUtil.PrepareControlForExport(current);
            }
            i = i + 1;
        }

    }
}