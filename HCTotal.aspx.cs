﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using OfficeOpenXml;
using OfficeOpenXml.Style;
public partial class _Default : System.Web.UI.Page
{
    data sqlquery = new data();
    DataTable tabla = new DataTable();

    

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {

            GridView1.DataSource = sqlquery.get_inventario("select (H.linea),h.turno, H.depto, (select count(HC.numreloj) from HEADCOUNT HC where h.depto=hc.depto and h.linea=hc.linea and h.Turno=hc.Turno)'HC', (select sum(HC.asistencia) from HEADCOUNT HC where h.depto=hc.depto and h.linea=hc.linea and h.Turno=hc.Turno)'HC asistencias', (select count(HC.numreloj) from HEADCOUNT HC where hc.tipoemp='1' and h.depto=hc.depto and h.linea=hc.linea and h.Turno=hc.Turno)'Directos', (select count(HC.numreloj) from HEADCOUNT HC where (hc.TipoEmp='2' or hc.TipoEmp='5' ) and h.depto=hc.depto and h.linea=hc.linea and h.Turno=hc.Turno)'Indirectos' , (select count(HC.numreloj) from HEADCOUNT HC where hc.tipoemp='1' and asistencia=1 and h.depto=hc.depto and h.linea=hc.linea and h.Turno=hc.Turno)'Directos Asistencia', (select count(HC.numreloj) from HEADCOUNT HC where (hc.TipoEmp='2' or hc.TipoEmp='5') and hc.asistencia=1 and h.depto=hc.depto and h.linea=hc.linea and h.Turno=hc.Turno)'Indirectos asist'  from headcount H group by H.depto,h.Linea,h.turno order by depto,linea");
            GridView1.DataBind();
            

            GridView2.DataSource = sqlquery.get_inventario("select distinct (H.Turno), (select count(HC.numreloj) from HEADCOUNT HC where H.turno=hc.Turno )'HC', (select sum(HC.asistencia) from HEADCOUNT HC where H.turno=hc.Turno)'HC asistencias', (select count(HC.numreloj) from HEADCOUNT HC where hc.tipoemp='1' and H.turno=hc.Turno)'Directos' , (select count(HC.numreloj) from HEADCOUNT HC where hc.tipoemp='1' and asistencia=1 and H.turno=hc.Turno)'Directos Asistencia',  (select count(HC.numreloj) from HEADCOUNT HC where (hc.TipoEmp='2' or hc.TipoEmp='2') and H.turno=hc.Turno)'Indirectos' ,(select count(HC.numreloj) from HEADCOUNT HC where (hc.TipoEmp='2' or hc.TipoEmp='2') and hc.asistencia=1 and H.turno=hc.Turno)'Indirectos asist', (select count(HC.numreloj) from HEADCOUNT HC where (hc.TipoEmp='5' ) and H.turno=hc.Turno)'Salary' ,(select count(HC.numreloj) from HEADCOUNT HC where ( hc.TipoEmp='5') and hc.asistencia=1 and H.turno=hc.Turno)'Salary asist'  from headcount H order by turno");
            GridView2.DataBind();
            

        }

           
        try
        {
            if (Session["acceso"].ToString().Trim() == "Administrador" || Session["acceso"].ToString().Trim() == "Usuario")
            {

            }
            else 
            {
                Response.Redirect("Login.aspx");
            }
        }
        catch (Exception)
        {
            Response.Redirect("Login.aspx");
        }
       
        


    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        
        
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
       

    }
    protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void GridView1_SelectedIndexChanged1(object sender, EventArgs e)
    {

    }

    protected void GridView1_RowCommand(object sender, CommandEventArgs e)
    {

      
    }
    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
    }
    protected void Button3_Click(object sender, EventArgs e)
    {
           }
}