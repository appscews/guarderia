﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using OfficeOpenXml;
using OfficeOpenXml.Style;
public partial class _Default : System.Web.UI.Page
{
    data sqlquery = new data();
    DataTable tabla = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["acceso"].ToString().Trim() != "Administrador")
            {
                Response.Redirect("Login.aspx");
            }
        }
        catch (Exception)
        {
            Response.Redirect("Login.aspx");
        }
        if (TextBox2.Text.Length <= 0)
        {
            
            string myvar = DateTime.Now.ToString();
            myvar = myvar.Substring(0, 9);
            tabla = sqlquery.regresa_query("select A.FECHA_CHQ, A.HORA_CHQ, (select B.numemp from Empleado B where A.emp_id = B.emp_id ) 'NUM_RELOJ',  A.CPL_ID  from carga A where A.FECHA_CHQ >= '" + myvar + "'  order by fecha_chq , hora_chq ");
            GridView1.DataSource = tabla;
            GridView1.DataBind();
           // GridView1.Attributes.Add("style", "table-layout:fixed");
          //  GridView1.AutoGenerateColumns = true;
        }
        


    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        string var = sqlquery.regresanumreloj(TextBox1.Text.Trim());
        if (var.Length > 0)
        {
            tabla = sqlquery.regresa_query("select A.FECHA_CHQ, A.HORA_CHQ, (select B.numemp from Empleado B where A.emp_id = B.emp_id ) 'NUM_RELOJ'  from carga A where   A.emp_id='" + var + "' and A.fecha_chq  between '" + TextBox2.Text.Trim() + "' and '" + TextBox3.Text.Trim() + "' order by fecha_chq  ,  hora_chq ");
        }
        else 
        {
            tabla = sqlquery.regresa_query("select A.FECHA_CHQ, A.HORA_CHQ, (select B.numemp from Empleado B where A.emp_id = B.emp_id ) 'NUM_RELOJ'  from carga A where    A.fecha_chq  between '" + TextBox2.Text.Trim() + "' and '" + TextBox3.Text.Trim() + "' order by fecha_chq  ,  hora_chq ");
        }
        GridView1.DataSource = tabla;
        GridView1.DataBind();
       // GridView1.Attributes.Add("style", "table-layout:fixed");
       // GridView1.AutoGenerateColumns = true;

    }

    protected void GridView1_RowCommand(object sender, CommandEventArgs e)
    {

    }

    protected void GridView1_SelectedIndexChanged1(object sender, EventArgs e)
    {

    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        ExcelPackage pck = new ExcelPackage();
        string var = sqlquery.regresanumreloj(TextBox1.Text.Trim());
        if (var.Length > 0)
        {
            tabla = sqlquery.regresa_query("select A.FECHA_CHQ, A.HORA_CHQ, (select B.numemp from Empleado B where A.emp_id = B.emp_id ) 'NUM_RELOJ'  from carga A where   A.emp_id='" + var + "' and A.fecha_chq  between '" + TextBox2.Text.Trim() + "' and '" + TextBox3.Text.Trim() + "' order by fecha_chq  ,  hora_chq ");
        }
        else
        {
            tabla = sqlquery.regresa_query("select A.FECHA_CHQ, A.HORA_CHQ, (select B.numemp from Empleado B where A.emp_id = B.emp_id ) 'NUM_RELOJ'  from carga A where    A.fecha_chq  between '" + TextBox2.Text.Trim() + "' and '" + TextBox3.Text.Trim() + "' order by fecha_chq  ,  hora_chq ");
        }
        GridView1.DataSource = tabla;
        GridView1.DataBind();
        
        var ws = pck.Workbook.Worksheets.Add("Asistencia");

        ws.Cells[1, 1].Value = "Fecha";//"Num-Parte";
        ws.Cells[1, 2].Value = "Hora";
        ws.Cells[1, 3].Value = "Num Reloj";
        ws.Cells[1, 4].Value = "checador";
        

        ws.Cells[1, 1].Style.Font.Bold = true;
        ws.Cells[1, 2].Style.Font.Bold = true;
        ws.Cells[1, 3].Style.Font.Bold = true;
        ws.Cells[1, 4].Style.Font.Bold = true;
        ws.Cells[1, 5].Style.Font.Bold = true;


        int x = 2, y = 1;
        for (int row = 0; row <= tabla.Rows.Count - 1; row++)
        {
            for (int cell = 0; cell <= tabla.Columns.Count - 1; cell++)
            {
                ws.Cells[x, y].Value = tabla.Rows[row][cell].ToString();
                y++;
            }
            y = 1;
            x++;
        }
        ws.Column(1).AutoFit();
        ws.Column(2).AutoFit();
        ws.Column(3).AutoFit();
        ws.Column(4).AutoFit();
        ws.Column(5).AutoFit();

        pck.SaveAs(Response.OutputStream);
        //Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        Response.ContentType = "application/vnd.ms-excel";
        Response.AddHeader("content-disposition", "attachment;  filename=ReporteAsistencia.xlsx");
        Response.End();

    }
}