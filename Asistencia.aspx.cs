﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using OfficeOpenXml;
using OfficeOpenXml.Style;
public partial class _Default : System.Web.UI.Page
{
    data sqlquery = new data();
    DataTable tabla = new DataTable();
    string query;
    

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            try { Label1.Text = sqlquery.get_cadena("Select top 1 id from actualizacion order by fecha desc") + " " + sqlquery.get_cadena("Select top 1 fecha from actualizacion order by fecha desc"); }
            catch (Exception)
            { }

            DropDownList1.Items.Clear();

            DropDownList1.AppendDataBoundItems = true;
            tabla = sqlquery.get_inventario("select distinct(Depto)as 'Depto' from HEADCOUNT  order by Depto");
            DropDownList1.DataSource = tabla;
            DropDownList1.DataTextField = "Depto";
            DropDownList1.DataValueField = "Depto";
            DropDownList1.DataBind();
            DropDownList1.Items.Insert(0, "Departamento");

            DropDownList2.Items.Insert(0, "Area");
            
            DropDownList3.Items.Insert(0, "Turno");
            DropDownList3.Items.Insert(1, "1");
            DropDownList3.Items.Insert(2, "2");
            DropDownList3.Items.Insert(3, "3");
           
            DropDownList4.Items.Insert(0, "Tipo");
            DropDownList4.Items.Insert(1, "1");
            DropDownList4.Items.Insert(2, "2");
            DropDownList4.Items.Insert(3, "5");

            GridView1.DataSource = sqlquery.get_inventario("select * from headcount order by numreloj");

            //string script = "alert(" + sqlquery.get_inventario("select * from headcount order by numreloj") +");";
            //ScriptManager.RegisterStartupScript(this, GetType(),
            //                      "ServerControlScript", script, true);
            GridView1.DataBind();
            query = "select * from headcount order by numreloj";
           // GridView1.Attributes.Add("style", "table-layout:fixed");
            GridView1.AutoGenerateColumns = true;
        }

            string myvar = DateTime.Now.ToString();
            myvar = myvar.Substring(0, 9);
		//Button2.Text=myvar;
        try
        {
            if (Session["acceso"].ToString().Trim() == "Administrador" || Session["acceso"].ToString().Trim() == "Usuario")
            {

            }
            else 
            {
                Response.Redirect("Login.aspx");
            }
        }
        catch (Exception)
        {
            Response.Redirect("Login.aspx");
        }
       
        


    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        
        
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        ExcelPackage pck = new ExcelPackage();
       // string var = sqlquery.regresanumreloj(TextBox1.Text.Trim());

        tabla = sqlquery.get_inventario(query);
        query = "select * from headcount";
        
        int a = int.Parse(DropDownList1.SelectedIndex.ToString()) + int.Parse(DropDownList2.SelectedIndex.ToString()) + int.Parse(DropDownList3.SelectedIndex.ToString()) + int.Parse(DropDownList4.SelectedIndex.ToString());
        int b = 0;
        if (a > 0)
        {
            query = query + " where ";
            if (DropDownList1.SelectedIndex != 0)
            {
                query = query + " Depto='" + DropDownList1.SelectedValue.ToString().Trim() + "'";
                b = b + 1;
            }

            if (DropDownList2.SelectedIndex != 0)
            {
                if (b > 0)
                {
                    query = query + " and ";
                }
                query = query + " linea='" + DropDownList2.SelectedValue.ToString().Trim() + "'";
                b++;

            }

            if (DropDownList3.SelectedIndex != 0)
            {
                if (b > 0)
                {
                    query = query + " and ";
                }
                query = query + " Turno='" + DropDownList3.SelectedValue.ToString().Trim() + "'";
                b++;
            }

            if (DropDownList4.SelectedIndex != 0)
            {
                if (b > 0)
                {
                    query = query + " and ";
                }
                query = query + " tipoemp='" + DropDownList4.SelectedValue.ToString().Trim() + "'";
                b++;
            }


        }
        query = query + " order by numreloj";
        tabla = sqlquery.get_inventario(query);
        
        var ws = pck.Workbook.Worksheets.Add("Asistencia");
        

        //ws.Cells[1, 1].Value = "Fecha";//"Num-Parte";
        //ws.Cells[1, 2].Value = "Hora";
        ws.Cells[1, 1].Value = "Num Reloj";
        ws.Cells[1, 2].Value = "Nombre";
        ws.Cells[1, 3].Value = "Ingreso";
        ws.Cells[1, 4].Value = "Turno";
        ws.Cells[1, 5].Value = "Directo/Indirecto";
        ws.Cells[1, 6].Value = "Puesto";
        ws.Cells[1, 7].Value = "Area";
        ws.Cells[1, 8].Value = "Depto";
        ws.Cells[1, 9].Value = "Asistencia";
        //ws.Cells[1, 4].Value = "checador";
        

        ws.Cells[1, 1].Style.Font.Bold = true;
        ws.Cells[1, 2].Style.Font.Bold = true;
        ws.Cells[1, 3].Style.Font.Bold = true;
        ws.Cells[1, 4].Style.Font.Bold = true;
        ws.Cells[1, 5].Style.Font.Bold = true;


        int x = 2, y = 1;
        for (int row = 0; row <= tabla.Rows.Count - 1; row++)
        {
            for (int cell = 0; cell <= tabla.Columns.Count - 1; cell++)
            {
                ws.Cells[x, y].Value = tabla.Rows[row][cell];
                y++;
            }
            y = 1;
            x++;
        }
        ws.Column(1).AutoFit();
        ws.Column(2).AutoFit();
        ws.Column(3).AutoFit();
        ws.Column(4).AutoFit();
        ws.Column(5).AutoFit();
        ws.Column(6).AutoFit();
        ws.Column(7).AutoFit();
        ws.Column(8).AutoFit();

        pck.SaveAs(Response.OutputStream);
        //Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        Response.ContentType = "application/vnd.ms-excel";
        Response.AddHeader("content-disposition", "attachment;  filename=ReporteAsistencia.xlsx");
        Response.End();
   

    }
    protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void GridView1_SelectedIndexChanged1(object sender, EventArgs e)
    {

    }

    protected void GridView1_RowCommand(object sender, CommandEventArgs e)
    {


        int index = Convert.ToInt32(e.CommandArgument);


        string llave = GridView1.Rows[index].Cells[1].Text.Trim();
        

      
    }
    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList2.Items.Clear();
        
        DropDownList2.AppendDataBoundItems = true;
        tabla = sqlquery.get_inventario("select distinct(linea)as 'Linea' from HEADCOUNT where depto='" + DropDownList1.SelectedValue.ToString() + "'  order by Linea");
        DropDownList2.DataSource = tabla;
        DropDownList2.DataTextField = "Linea";
        DropDownList2.DataValueField = "Linea";
        DropDownList2.DataBind();
        DropDownList2.Items.Insert(0, "Linea");
    }
    protected void Button3_Click(object sender, EventArgs e)
    {
        query= "select * from headcount";
      
        int a = int.Parse(DropDownList1.SelectedIndex.ToString()) + int.Parse(DropDownList2.SelectedIndex.ToString()) + int.Parse(DropDownList3.SelectedIndex.ToString()) + int.Parse(DropDownList4.SelectedIndex.ToString());
        int b=0;
        if (a>0)
        {
            query = query + " where ";
            if (DropDownList1.SelectedIndex != 0)
            {
                query= query + " Depto='"+ DropDownList1.SelectedValue.ToString().Trim()+ "'";
                b = b + 1;
            }

            if (DropDownList2.SelectedIndex != 0)
            {
                if (b > 0)
                {
                    query = query + " and ";
                }
                query = query + " linea='" + DropDownList2.SelectedValue.ToString().Trim() + "'";
                b++;
                
            }

            if (DropDownList3.SelectedIndex != 0)
            {
                if (b > 0)
                {
                    query = query + " and ";
                }
                query = query + " Turno='" + DropDownList3.SelectedValue.ToString().Trim() + "'";
                b++;
            }

            if (DropDownList4.SelectedIndex != 0)
            {
                if (b > 0)
                {
                    query = query + " and ";
                }
                query = query + " tipoemp='" + DropDownList4.SelectedValue.ToString().Trim() + "'";
                b++;
            }


        }
        query=query + " order by numreloj";
        GridView1.DataSource = sqlquery.get_inventario(query);
        GridView1.DataBind();
    }
}