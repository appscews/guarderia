﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using OfficeOpenXml;
using OfficeOpenXml.Style;

public partial class Reporte : System.Web.UI.Page
{
    ExcelPackage pck = new ExcelPackage();
    data sqlquery = new data();
    DataTable tabla = new DataTable();
    
    

    
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            TextBox1.Text = TextBox2.Text = DateTime.Now.ToString("MM/dd/yyyy");
        }

        contenido.Visible = true;
        try
        {
            if (Session["acceso"].ToString().Trim() != "Administrador")
            {
                Response.Redirect("Login.aspx");
            }
        }
        catch (Exception)
        { Response.Redirect("Login.aspx"); 

        }
    }


    //protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
    //{
       
        //if (RadioButtonList1.SelectedIndex == 0) { Button1.Visible = true; Button2.Visible = false; Button3.Visible = false; contenido.Visible = false; }
        //else if (RadioButtonList1.SelectedIndex == 1) { consulta=2; }// /*Button2.Visible = true;*/ Button1.Visible = false; Button3.Visible = false; contenido.Visible = true; }
        //else if (RadioButtonList1.SelectedIndex == 2) { consulta=3; }///*Button3.Visible = true;*/ Button1.Visible = false; Button2.Visible = false; contenido.Visible = true; }
    //}


    protected void Button2_Click(object sender, EventArgs e)
    {
        try
        {
            string filename, macropath;
            string query = "";
            string filtro = "";
            if (CheckBox1.Checked == true)
            {
                filtro = " and I.id=" + TextBox3.Text.Trim();
            }


            if (DropDownList1.SelectedIndex == 0)
            {
                query = "select (select top 1 fecha from asistencia asi where asi.id=a.id and asi.dia= a.dia)'Fecha', A.id, (select top 1 nombre from infantes inf where inf.id=A.id )'Nombre', (select top 1 edad from infantes inf where inf.id=A.id )'Edad',(select top 1 Num_Reloj from infantes inf where inf.id=A.id )'#Empleado CEWS', (select top 1 tutore from infantes inf where inf.id=A.id )'Empleado CEWS'  from asistencia A join infantes i on a.id=I.id where CONVERT(DATE,fecha) between CONVERT(DATE,'" + TextBox1.Text.Trim() + " 00:00:00') and CONVERT(DATE,'" + TextBox2.Text.Trim() + " 23:59:59') " + filtro + " group by a.dia, a.id order by fecha, id";
            }

            if (DropDownList1.SelectedIndex == 1)
            {
                query = "select ID, Nombre, Edad, Num_reloj, TutorE'Tutor CEWS', Tel1 'Tel', TutorA 'Tutor Autorizado',Tel2 'Tel', vigencia  from infantes I where activo=1   " + filtro;
            }

            if (DropDownList1.SelectedIndex == 2)
            {
                query = "select P.PagoID'#Folio' ,P.ID, I.nombre, P.Fecha_pago'Fecha de Pago', P.vigencia, p.cantidad'Importe'  from pagos P join Infantes I on I.id=p.id where CONVERT(DATE,p.fecha_pago) between CONVERT(DATE,'" + TextBox1.Text.Trim() + " 00:00:00') and CONVERT(DATE,'" + TextBox2.Text.Trim() + " 23:59:59') " + filtro;
            }

            GridView1.DataSource = sqlquery.get_inventario(query);
            GridView1.DataBind();
            if (GridView1.Rows.Count > 0)
            {
                filename = "Reporte_" + DropDownList1.SelectedValue.ToString() + ".xls";
                macropath = "http://" + Page.Request.Url.Host + ":" + Page.Request.Url.Port.ToString() + Page.Request.ApplicationPath + "/Report.mso".ToString();
                GridViewExportUtil.export(filename, this.GridView1, macropath);
            }
            
        }
        catch (Exception E)
        {
        }

    }
    
    protected void TextBox2_TextChanged(object sender, EventArgs e)
    {

    }


    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DropDownList1.SelectedIndex == 0)
        {
            Label2.Visible = Label3.Visible = Label4.Visible = TextBox1.Visible = TextBox2.Visible = TextBox3.Visible = CheckBox1.Visible = true;

        }
    }
}