﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using OfficeOpenXml;
using OfficeOpenXml.Style;
public partial class _Default : System.Web.UI.Page
{
    data sqlquery = new data();
    DataTable tabla = new DataTable();
    string query;

    void updategrid(string id)
    {

        
        //Image2.Height = 150;
        GridView1.DataSource = sqlquery.get_inventario("select P.ID,(select I.nombre from infantes I where P.ID=I.ID )'Nombre',P.PagoID,P.vigencia, P.cantidad from Pagos P where PagoID=" + id + " ");
        GridView1.DataBind();
        GridView1.Attributes.Add("style", "table-layout:fixed");
        GridView1.AutoGenerateColumns = true;
        Datos.Visible = botones.Visible = true;
        TextBox1.Text = GridView1.Rows[0].Cells[1].Text.Trim();
        TextBox2.Text = GridView1.Rows[0].Cells[2].Text.Trim();
        TextBox10.Text = GridView1.Rows[0].Cells[3].Text.Trim();
        TextBox11.Text = GridView1.Rows[0].Cells[4].Text.Trim();
        TextBox12.Text = GridView1.Rows[0].Cells[5].Text.Trim();
        // Image2.ImageUrl = "~/Fotos/" + TextBox1.Text.Trim() + ".jpg";
        GridView1.Visible = false;
        //Image2.Visible = true;

        return;
    }

    void loadgrid(string id)
    {


        
        GridView1.DataSource = sqlquery.get_inventario("select ID,Nombre,Num_Reloj'#Empleado CEWS',Edad,Tutore'Tutor CEWS', Tel1'Telefono', Tutora'Tutor 2', tel2'Telefono2' from infantes where activo=1 order by ID");
        GridView1.DataBind();
        GridView1.Attributes.Add("style", "table-layout:fixed");
        GridView1.AutoGenerateColumns = true;

        Image2.Visible = false;
        GridView1.Visible = true;

        return;
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
           
            Datos.Visible = false;
            botones.Visible = false;
            TextBox9.Focus();
        }
        try
        {
            if (Session["acceso"].ToString().Trim() == "Administrador" || Session["acceso"].ToString().Trim() == "Usuario")
            {
                GridView1.DataSource = sqlquery.get_inventario("select top 100 P.PagoID, P.ID,(select I.nombre from infantes I where P.ID=I.ID )'Nombre',P.vigencia, P.cantidad from Pagos P order by PagoID desc, fecha_pago desc");
                GridView1.DataBind();
                GridView1.Attributes.Add("style", "table-layout:fixed");
                GridView1.AutoGenerateColumns = true;
                
            }
            else 
            {
                Response.Redirect("Login.aspx");
            }
        }
        catch (Exception)
        {
            Response.Redirect("Login.aspx");
        }
       
        


    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        
        
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        try
        {
            DateTime.Parse(TextBox11.Text.Trim());
            if (TextBox1.Text.Length > 0 && TextBox2.Text.Length > 0 && TextBox10.Text.Length > 0 && TextBox11.Text.Length > 0 && TextBox12.Text.Length > 0)
            {
                


                if (sqlquery.get_inventario("select * from infantes where activo=1 and id=" + TextBox1.Text.Trim()).Rows.Count > 0)
                {
                    string idp = sqlquery.get_cadena("Select id from pagos where id=" + TextBox10.Text.Trim());
                    string vigenciap = sqlquery.get_cadena("Select vigenciaprevia from pagos where id=" + TextBox10.Text.Trim());
                    string vigenciapagos = sqlquery.get_cadena("Select top 1 vigencia from pagos where id=" + idp + " order by vigencia desc");
                    string cantidadp = sqlquery.get_cadena("Select top 1 cantidad from pagos where pagoid=" + TextBox10.Text.Trim() + " order by vigencia desc");


                    sqlquery.get_inventario("update pagos set Id=" + TextBox1.Text.Trim() + ", vigencia='" + TextBox11.Text.Trim() + "',cantidad=" + TextBox12.Text.Trim() + " where pagoid=" + TextBox10.Text.Trim());
                    sqlquery.get_inventario("update infantes set Vigencia='" + vigenciap + "' where id=" + idp);
                    sqlquery.get_inventario("update infantes set Vigencia='" + vigenciapagos + "' where id=" + idp);
                    sqlquery.get_inventario("update infantes set Vigencia='" + TextBox11.Text.Trim() + "' where id=" + TextBox1.Text.Trim());

                    sqlquery.get_inventario("insert into Cambios values(" + TextBox10.Text.Trim() + "," + idp + "," + TextBox1.Text.Trim() + ",'" + vigenciap + "','" + TextBox11.Text.Trim() + "',sysdatetime(),'Cambio'," + cantidadp + "," + TextBox12.Text.Trim() + ")");
                    
                    //Response.Redirect("Cambios.aspx");
                    loadgrid("1 or 1=1");
                    Label1.Text = "Pago Modificado";
                    TextBox1.Text = TextBox2.Text = TextBox10.Text = TextBox11.Text = TextBox12.Text = "";
                    Datos.Visible = false;
                }
                else
                {
                    Label1.Text = "Codigo de Niño no existe.";
                }


            }
            else
            {
                Label1.Text = "Favor de llenar todos los campos obligatorios *.";
            }
        }
        catch (Exception)
        {
            Label1.Text = "Formato de fecha incorrecto.";
        }
    }
    protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void GridView1_SelectedIndexChanged1(object sender, EventArgs e)
    {

    }

    protected void GridView1_RowCommand(object sender, CommandEventArgs e)
    {


        int index = Convert.ToInt32(e.CommandArgument);


        string llave = GridView1.Rows[index].Cells[1].Text.Trim();

        updategrid(llave);


    }
    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        
    }
    protected void Button3_Click(object sender, EventArgs e)
    {
        
    }

    protected void Button4_Click(object sender, EventArgs e)
    {
        if (sqlquery.get_inventario("select P.ID,(select I.nombre from infantes I where P.ID=I.ID )'Nombre',P.PagoID,P.vigencia, P.cantidad from Pagos P where PagoID=" + TextBox9.Text.Trim() + " ").Rows.Count > 0)
        {
            updategrid(TextBox9.Text.Trim());
            TextBox9.Text = "";
        }
        else
        {
            TextBox9.Text = "";
            TextBox9.Focus();
        }
    }
}