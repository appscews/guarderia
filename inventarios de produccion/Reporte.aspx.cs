﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using OfficeOpenXml;
using OfficeOpenXml.Style;

public partial class Reporte : System.Web.UI.Page
{
    ExcelPackage pck = new ExcelPackage();
    data sqlquery = new data();
    DataTable tabla = new DataTable();
    
    

    
    
    protected void Page_Load(object sender, EventArgs e)
    {
        CalendarExtender1.SelectedDate = DateTime.Now;
        CalendarExtender2.SelectedDate = DateTime.Now;
        Label3.Visible = Label4.Visible = TextBox1.Visible = TextBox2.Visible = true;
        contenido.Visible = true;
    }


    //protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
    //{
       
        //if (RadioButtonList1.SelectedIndex == 0) { Button1.Visible = true; Button2.Visible = false; Button3.Visible = false; contenido.Visible = false; }
        //else if (RadioButtonList1.SelectedIndex == 1) { consulta=2; }// /*Button2.Visible = true;*/ Button1.Visible = false; Button3.Visible = false; contenido.Visible = true; }
        //else if (RadioButtonList1.SelectedIndex == 2) { consulta=3; }///*Button3.Visible = true;*/ Button1.Visible = false; Button2.Visible = false; contenido.Visible = true; }
    //}

    protected void Button1_Click(object sender, EventArgs e)
    {
        string filename, macropath;
        GridView1.DataSource = sqlquery.get_inventario("SELECT  p.PNum_Parte,i.Cantidad,c.Descripcion,floor(min(p.PCantidad)/c.Can_Atado)*c.Can_Atado 'Cables en produccion' " +
                                                          "FROM [IProduccion].[dbo].[PInventario] p " +
                                                          "join [inventarios].[dbo].[Catalogo] c on c.Num_parte=p.PNum_Parte " +
                                                          "join [inventarios].[dbo].[Inventario] i on i.Num_parte=p.PNum_Parte " +
                                                          "where c.Activo=1 " +
                                                          "group by c.Descripcion,p.PNum_Parte,c.Can_Atado,i.Cantidad " +
                                                          "order by 1 "); 
        GridView1.DataBind();
        GridView1.Visible = false;

        if (GridView1.Rows.Count > 0){}

                    filename = "Reporte_inventario.xls";
                    macropath = "http://" + Page.Request.Url.Host+ ":" + Page.Request.Url.Port.ToString() + Page.Request.ApplicationPath + "/Report.mso".ToString();
                    GridViewExportUtil.export(filename, this.GridView1, macropath);
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
               tabla = sqlquery.get_inventario("select c.Num_Parte, a.Fechahora ,a.Empleado,Operacion " +
       "from operaciones a " +
       "join Catalogo c on a.Num_parte=c.Codigo_Barras where fecha between'" + TextBox1.Text + " 00:00:00'"
       + " and '" + TextBox2.Text + " 23:59:59'");
               var ws = pck.Workbook.Worksheets.Add("Altas y bajas");

               ws.Cells[1, 1].Value = "Set Code";
               ws.Cells[1, 2].Value = "Fecha";
               ws.Cells[1, 3].Value = "Empleado";
               ws.Cells[1, 4].Value = "Operacion";
               ws.Cells[1, 1].Style.Font.Bold = true;
               ws.Cells[1, 2].Style.Font.Bold = true;
               ws.Cells[1, 3].Style.Font.Bold = true;
               ws.Cells[1, 4].Style.Font.Bold = true;

               int x = 2, y = 1;
               for (int row = 0; row <= tabla.Rows.Count - 1; row++)
               {
                   for (int cell = 0; cell <= tabla.Columns.Count - 1; cell++)
                   {
                       ws.Cells[x, y].Value = tabla.Rows[row][cell].ToString();
                       y++;
                   }
                   y = 1;
                   x++;
               }
               ws.Column(1).AutoFit();
               ws.Column(2).AutoFit();
               ws.Column(3).AutoFit();
               ws.Column(4).AutoFit();


               tabla = sqlquery.get_inventario("select c.Num_Parte, a.Fecha, COUNT (*) #Operaciones from operaciones a join Catalogo c on a.Num_parte=c.Codigo_Barras " +
                   "where fecha between'" + TextBox1.Text + " 00:00:00'"
       + " and '" + TextBox2.Text + " 23:59:59' " +
       "group by c.Num_Parte,a.Fecha ");

               ws = pck.Workbook.Worksheets.Add("conteo por numparte");

               ws.Cells[1, 1].Value = "Set Code";
               ws.Cells[1, 2].Value = "Fecha";
               ws.Cells[1, 3].Value = "#Operaciones";
               ws.Cells[1, 1].Style.Font.Bold = true;
               ws.Cells[1, 2].Style.Font.Bold = true;
               ws.Cells[1, 3].Style.Font.Bold = true;


               x = 2; y = 1;
               for (int row = 0; row <= tabla.Rows.Count - 1; row++)
               {
                   for (int cell = 0; cell <= tabla.Columns.Count - 1; cell++)
                   {
                       ws.Cells[x, y].Value = tabla.Rows[row][cell].ToString();
                       y++;
                   }
                   y = 1;
                   x++;
               }
               ws.Column(1).AutoFit();
               ws.Column(2).AutoFit();
               ws.Column(3).AutoFit();

               tabla = sqlquery.get_inventario("select a.Empleado,a.Fecha,COUNT (*) from operaciones a join Catalogo c on a.Num_parte=c.Codigo_Barras " +
                   "where fecha between'" + TextBox1.Text + " 00:00:00'"
       + " and '" + TextBox2.Text + " 23:59:59' " +
       "group by a.Empleado,a.Fecha ") ;


               ws = pck.Workbook.Worksheets.Add("conteo por empleado");

               ws.Cells[1, 1].Value = "Empleado";
               ws.Cells[1, 2].Value = "Fecha";
               ws.Cells[1, 3].Value = "#Operaciones";
               ws.Cells[1, 1].Style.Font.Bold = true;
               ws.Cells[1, 2].Style.Font.Bold = true;
               ws.Cells[1, 3].Style.Font.Bold = true;


               x = 2; y = 1;
               for (int row = 0; row <= tabla.Rows.Count - 1; row++)
               {
                   for (int cell = 0; cell <= tabla.Columns.Count - 1; cell++)
                   {
                       ws.Cells[x, y].Value = tabla.Rows[row][cell].ToString();
                       y++;
                   }
                   y = 1;
                   x++;
               }
               ws.Column(1).AutoFit();
               ws.Column(2).AutoFit();
               ws.Column(3).AutoFit();

               pck.SaveAs(Response.OutputStream);
               //Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
               Response.ContentType = "application/vnd.ms-excel";
               Response.AddHeader("content-disposition", "attachment;  filename=Report.xlsx");
               Response.End();
    }
    protected void Button3_Click(object sender, EventArgs e)
    {
        //tabla = sqlquery.get_inventario("Select * from cambios_inventario order by 1 where fecha between'" + TextBox1.Text + " 00:00:00'"  + " and '" + TextBox2.Text + " 23:59:59' "  '");
        tabla = sqlquery.get_inventario("select * " +
       "from Cambio a " +
       "where fecha between'" + TextBox1.Text + " 00:00:00'"
       + " and '" + TextBox2.Text + " 23:59:59'");
	



        var ws = pck.Workbook.Worksheets.Add("CambiosInventario");

        ws.Cells[1, 1].Value = "Set_Code";//"Num-Parte";
        ws.Cells[1, 2].Value = "Can_Inicial";
        ws.Cells[1, 3].Value = "Can_Final";
        ws.Cells[1, 4].Value = "Motivo";
        ws.Cells[1, 5].Value = "Fecha";

        ws.Cells[1, 1].Style.Font.Bold = true;
        ws.Cells[1, 2].Style.Font.Bold = true;
        ws.Cells[1, 3].Style.Font.Bold = true;
        ws.Cells[1, 4].Style.Font.Bold = true;
        ws.Cells[1, 5].Style.Font.Bold = true;
        

        int x = 2, y = 1;
        for (int row = 0; row <= tabla.Rows.Count - 1; row++)
        {
            for (int cell = 0; cell <= tabla.Columns.Count - 1; cell++)
            {
                ws.Cells[x, y].Value = tabla.Rows[row][cell].ToString();
                y++;
            }
            y = 1;
            x++;
        }
        ws.Column(1).AutoFit();
        ws.Column(2).AutoFit();
        ws.Column(3).AutoFit();
        ws.Column(4).AutoFit();
        ws.Column(5).AutoFit();

        pck.SaveAs(Response.OutputStream);
        //Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        Response.ContentType = "application/vnd.ms-excel";
        Response.AddHeader("content-disposition", "attachment;  filename=Report.xlsx");
        Response.End();
    }

}