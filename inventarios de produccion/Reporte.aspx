﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Reporte.aspx.cs" Inherits="Reporte" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <div style=" margin:10px 10px 10px 10px">

        <br />
        <br />
        <div id="contenido" runat="server" visible="false">
        <table>
            <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            </tr>
            <tr>
            <td>
                <asp:Label ID="Label3" runat="server" Text="Fecha Inicio:"></asp:Label>
                </td>
            <td>
                <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="TextBox1" PopupButtonID="TextBox1" Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
            <td>
                <asp:Label ID="Label4" runat="server" Text="Fecha Final:"></asp:Label>
                </td>
            <td>
                <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="TextBox2" PopupButtonID="TextBox2" Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
            </td>
            </tr>
            <tr>
            <td>&nbsp;</td>
            <td align="right">
                &nbsp;</td>
                </tr>
        </table>
        </div>
        <asp:Button ID="Button1" runat="server" Text="Exportar inventario a Excel" 
            onclick="Button1_Click" />
        <asp:Button ID="Button2" runat="server" Text="Reporte de Altas,bajas" 
            onclick="Button2_Click" />
        <asp:Button ID="Button3" runat="server" Text="Reporte de cambios" 
            onclick="Button3_Click" />
        <br />
        <asp:GridView ID="GridView1" runat="server" BackColor="Red" 
            EmptyDataText="empty" Visible="False">
        </asp:GridView>
        <br />
    </div>
</asp:Content>

