﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Inventario : System.Web.UI.Page
{
    data sqlquery = new data();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            string temp = Session["acceso"].ToString();
            if (Session["acceso"].ToString().Trim() == "Usuario" || Session["acceso"].ToString().Trim() == "Usuario con privilegios") LinkButton2.Visible = false;
            else LinkButton2.Visible = true;

            GridView1.DataSource = sqlquery.get_inventario("SELECT Inventario.Num_Parte , Inventario.Cantidad, Catalogo.Descripcion  FROM [Inventario] " +
            "Join Catalogo On Inventario.Num_Parte = Catalogo.Num_Parte WHERE Catalogo.Activo=1 ORDER BY [Num_Parte] ");
            GridView1.DataBind();
        }
    }

    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        string cadena;
        cadena = "SELECT * FROM [Inventario]  ORDER BY [Num_Parte]";
        if (int.Parse(DropDownList1.SelectedItem.Value) == 0)
        {
            cadena = "SELECT  p.PNum_Parte,i.Cantidad,c.Descripcion,floor(min(p.PCantidad)/c.Can_Atado)*c.Can_Atado 'Cables en produccion' "+
                                                          "FROM [IProduccion].[dbo].[PInventario] p "+
                                                          "join [inventarios].[dbo].[Catalogo] c on c.Num_parte=p.PNum_Parte "+
                                                          "join [inventarios].[dbo].[Inventario] i on i.Num_parte=p.PNum_Parte "+
                                                          "where c.Activo=1 "+
                                                          "group by c.Descripcion,p.PNum_Parte,c.Can_Atado,i.Cantidad "+
                                                          "order by 1 ";
        }
        if (int.Parse(DropDownList1.SelectedItem.Value) == 1)
        {
            cadena = "SELECT  p.PNum_Parte,i.Cantidad,c.Descripcion,c.Maximo,"+
"floor(min(p.PCantidad)/c.Can_Atado)*c.Can_Atado 'Cables en produccion' "+
"FROM [IProduccion].[dbo].[PInventario] p "+
"join [inventarios].[dbo].[Catalogo] c on c.Num_parte=p.PNum_Parte "+
"join [inventarios].[dbo].[Inventario] i on i.Num_parte=p.PNum_Parte "+
"WHERE (i.Cantidad >= c.Maximo AND c.Activo=1) "+
"group by c.Descripcion,p.PNum_Parte,c.Can_Atado,i.Cantidad,c.Maximo "+
"order by 1";
        }
        if (int.Parse(DropDownList1.SelectedItem.Value) == 2)
        {

            cadena = "SELECT  p.PNum_Parte,i.Cantidad,c.Descripcion,c.Minimo," +
"floor(min(p.PCantidad)/c.Can_Atado)*c.Can_Atado 'Cables en produccion' " +
"FROM [IProduccion].[dbo].[PInventario] p " +
"join [inventarios].[dbo].[Catalogo] c on c.Num_parte=p.PNum_Parte " +
"join [inventarios].[dbo].[Inventario] i on i.Num_parte=p.PNum_Parte " +
"WHERE (i.Cantidad <= c.Minimo AND c.Activo=1) "+
"group by c.Descripcion,p.PNum_Parte,c.Can_Atado,i.Cantidad,c.Minimo " +
"order by 1";
        }
        GridView1.DataSource = sqlquery.get_inventario(cadena);
        GridView1.DataBind();

    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        Response.Redirect("Inventario.aspx");
    }

    protected void LinkButton2_Click(object sender, EventArgs e)
    {
        Response.Redirect("CambiosInventario.aspx");
    }
}