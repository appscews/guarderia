﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using OfficeOpenXml;
using OfficeOpenXml.Style;
public partial class _Default : System.Web.UI.Page
{
    data sqlquery = new data();
    DataTable tabla = new DataTable();
    string query;
    

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
           
            Datos.Visible = false;
            botones.Visible = false;
        }
        try
        {
            if (Session["acceso"].ToString().Trim() == "Administrador" || Session["acceso"].ToString().Trim() == "Usuario")
            {
                GridView1.DataSource = sqlquery.get_inventario("select ID, Nombre,num_reloj'#Empleado CEWS',Edad,tutore'Tutor CEWS',Tel1'Telefono',tutora'Tutor 2', tel2'Telefono' from infantes where activo=1 order by ID");
                GridView1.DataBind();
                GridView1.Attributes.Add("style", "table-layout:fixed");
                GridView1.AutoGenerateColumns = true;
                
            }
            else 
            {
                Response.Redirect("Login.aspx");
            }
        }
        catch (Exception)
        {
            Response.Redirect("Login.aspx");
        }
       
        


    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        
        
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        if (TextBox1.Text.Length > 0 && TextBox2.Text.Length > 0 && TextBox3.Text.Length > 0 && TextBox5.Text.Length > 0)
        {
            GridView1.DataSource = sqlquery.get_inventario("select ID,Nombre,Num_Reloj'#Empleado CEWS',Edad,Tutore'Tutor CEWS', Tel1'Telefono', Tutora'Tutor 2', tel2'Telefono' from infantes where ID=" + GridView1.Rows[0].Cells[1].Text.Trim() + " order by ID");
            sqlquery.get_inventario("update infantes set Nombre='" + TextBox2.Text.Trim() + "',Num_reloj='" + TextBox3.Text.Trim() + "',edad=" + TextBox4.Text.Trim() + ", tutore='" + TextBox5.Text.Trim() + "',Tel1='" + TextBox6.Text.Trim() + "',tutora=' " + TextBox7.Text.Trim() + "', tel2='" + TextBox8.Text.Trim() + "' where id=" + TextBox1.Text.Trim() + " ");
            Response.Redirect("Cambios.aspx");
        }
        Label1.Text = "Favor de llenar todos los campos obligatorios *.";
    }
    protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void GridView1_SelectedIndexChanged1(object sender, EventArgs e)
    {

    }

    protected void GridView1_RowCommand(object sender, CommandEventArgs e)
    {


        int index = Convert.ToInt32(e.CommandArgument);


        string llave = GridView1.Rows[index].Cells[1].Text.Trim();
        
        Image2.Height = 150;
        GridView1.DataSource = sqlquery.get_inventario("select ID,Nombre,Num_Reloj'#Empleado CEWS',Edad,Tutore'Tutor CEWS', Tel1'Telefono', Tutora'Tutor 2', tel2'Telefono' from infantes where ID=" + GridView1.Rows[index].Cells[1].Text.Trim() + " order by ID");
        GridView1.DataBind();
        GridView1.Attributes.Add("style", "table-layout:fixed");
        GridView1.AutoGenerateColumns = true;
        Datos.Visible = botones.Visible= true;
        TextBox1.Text= GridView1.Rows[0].Cells[1].Text.Trim();
        TextBox2.Text = GridView1.Rows[0].Cells[2].Text.Trim();
        TextBox3.Text = GridView1.Rows[0].Cells[3].Text.Trim();
        TextBox4.Text = GridView1.Rows[0].Cells[4].Text.Trim();
        TextBox5.Text = GridView1.Rows[0].Cells[5].Text.Trim();
        TextBox6.Text = GridView1.Rows[0].Cells[6].Text.Trim();
        TextBox7.Text = GridView1.Rows[0].Cells[7].Text.Trim();
        TextBox8.Text = GridView1.Rows[0].Cells[8].Text.Trim();
        Image2.ImageUrl = "~/Fotos/" + TextBox1.Text.Trim() + ".jpg";
        GridView1.Visible = false;


    }
    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        
    }
    protected void Button3_Click(object sender, EventArgs e)
    {
        sqlquery.get_inventario("update infantes set Activo=0 where id=" + TextBox1.Text.Trim() + " ");
        Response.Redirect("Cambios.aspx");
    }
}