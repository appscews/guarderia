﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Cambios.aspx.cs" Inherits="_Default" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        .style3
        {width: 70px;}
        .style4{ width: 10px;}
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">

   
   
    <div class="container" >
   
        <br />

            <table align="center"> <tr><td>
                <asp:Image ID="Image2" runat="server" />
                </td></tr></table>

 
    <div style="overflow : auto; width : 100%; max-height : 800px;">
                <%--<asp:GridView ID="GridView1" runat="server" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3"--%> 
                <asp:GridView ID="GridView1" runat="server" class="table"
            
                    EmptyDataText="No hay incidencias con esa descripcion" GridLines="Vertical" 
                    onRowCommand="GridView1_RowCommand" 
                    onselectedindexchanged="GridView1_SelectedIndexChanged1">
                    <AlternatingRowStyle BackColor="#d9edf7" />
                    <Columns>
                        <asp:ButtonField Text="Select" />
                    </Columns>
                    <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                    <%--<HeaderStyle class="bg-primary" />--%>
              <HeaderStyle BackColor="#337ab7" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EEEEEE" ForeColor="Black" font-size="10px"/>
                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#0000A9" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#000065" />
                </asp:GridView>
        </div>
           <table id="Datos" runat="server" align="center"><tr><td>
                       <asp:Label ID="Label2" runat="server" autocomplete="off" BackColor="Black" ForeColor="White">#Control*</asp:Label>
    
        </td><td>
            <asp:TextBox ID="TextBox1" runat="server" MaxLength="10" Enabled="False"></asp:TextBox>
            <asp:FilteredTextBoxExtender ID="TextBox1_FilteredTextBoxExtender" runat="server" BehaviorID="TextBox1_FilteredTextBoxExtender" FilterType="Numbers" TargetControlID="TextBox1">
            </asp:FilteredTextBoxExtender>
        </td><td>
                       <asp:Label ID="Label3" runat="server" autocomplete="off" BackColor="Black" ForeColor="White">Nombre*</asp:Label>
    
            </td><td>
                <asp:TextBox ID="TextBox2" runat="server" MaxLength="50"></asp:TextBox>
                </td></tr><tr><td>
                       <asp:Label ID="Label4" runat="server" autocomplete="off" BackColor="Black" ForeColor="White">#EmpleadoCEWS*</asp:Label>
    
            </td><td>
                <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                <asp:FilteredTextBoxExtender ID="TextBox3_FilteredTextBoxExtender" FilterType="Numbers" runat="server" BehaviorID="TextBox3_FilteredTextBoxExtender" TargetControlID="TextBox3">
                </asp:FilteredTextBoxExtender>
            </td><td>
                       <asp:Label ID="Label5" autocomplete="off" BackColor="Black" ForeColor="White" runat="server">Edad</asp:Label>
    
            </td><td>
            <asp:TextBox ID="TextBox4" runat="server" MaxLength="2"></asp:TextBox>
            <asp:FilteredTextBoxExtender ID="TextBox4_FilteredTextBoxExtender" runat="server" BehaviorID="TextBox4_FilteredTextBoxExtender" FilterType="Numbers" TargetControlID="TextBox4">
            </asp:FilteredTextBoxExtender>
            </td></tr><tr><td>
                       <asp:Label ID="Label6" autocomplete="off" BackColor="Black" ForeColor="White" runat="server">TUTOR (CEWS)*</asp:Label>
    
            </td><td>
                <asp:TextBox ID="TextBox5" runat="server" MaxLength="50"></asp:TextBox>
            </td><td>
                       <asp:Label ID="Label7" autocomplete="off" BackColor="Black" ForeColor="White" runat="server">TELEFONO</asp:Label>
    
            </td><td>
                <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
                <asp:MaskedEditExtender ID="TextBox6_MaskedEditExtender" runat="server" BehaviorID="TextBox6_MaskedEditExtender" Century="2000" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder="" TargetControlID="TextBox6" Mask="999-999-9999" 
 AcceptNegative="None" MessageValidatorTip="true"  MaskType="Number" ClearMaskOnLostFocus="false" ClearTextOnInvalid="false">
                </asp:MaskedEditExtender>
            </td></tr>
        <tr>
            <td>
                       <asp:Label ID="Label8" autocomplete="off" BackColor="Black" ForeColor="White" runat="server">TUTOR 2</asp:Label>
    
            </td><td>
                <asp:TextBox ID="TextBox7" runat="server" MaxLength="50"></asp:TextBox>
            </td><td>
                       <asp:Label ID="Label9" runat="server" autocomplete="off" BackColor="Black" ForeColor="White">TELEFONO</asp:Label>
    
            </td><td>
                <asp:TextBox ID="TextBox8" runat="server"></asp:TextBox>
                <asp:MaskedEditExtender ID="TextBox8_MaskedEditExtender" runat="server" BehaviorID="TextBox8_MaskedEditExtender" Century="2000" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder="" TargetControlID="TextBox8" Mask="999-999-9999" 
 AcceptNegative="None" MessageValidatorTip="true"  MaskType="Number" ClearMaskOnLostFocus="false" ClearTextOnInvalid="false">
                </asp:MaskedEditExtender>
            </td>
        </tr></table><br />
                    <div class="row">
                          <table align="center" id="botones" runat="server"><tr><td><asp:Button ID="Button2" runat="server" class="btn btn-primary"
                        onclick="Button2_Click" Text="Guardar" /></td><td>&nbsp;&nbsp;</td><td><asp:Button ID="Button3" runat="server" class="btn btn-primary"
                        onclick="Button3_Click" Text="Dar de Baja" /></td></tr></table>
                    </div>
    
        <asp:ToolkitScriptManager ID="ToolkitScriptManager2" runat="server">
        </asp:ToolkitScriptManager>
    
                       <asp:Label ID="Label1" runat="server" autocomplete="off" BackColor="Black" ForeColor="White"></asp:Label>
    
        <br />
   
   </div>
  
</asp:Content>
