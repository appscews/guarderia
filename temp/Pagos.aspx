﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Pagos.aspx.cs" Inherits="_Default" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        .style3
        {width: 70px;}
        .style4{ width: 10px;}
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">

   
   
    <div class="container" >
   
        <br />
        <table align="right"> <tr><td>
			                          <asp:TextBox ID="TextBox9" name="form-username" placeholder="#Control" class="form-username form-control" runat="server" > </asp:TextBox> 
                                     
                                      <asp:FilteredTextBoxExtender ID="TextBox9_FilteredTextBoxExtender" FilterType="Numbers" runat="server" TargetControlID="TextBox9">
                                      </asp:FilteredTextBoxExtender>

                                     
                                    </td><td>
                
                <asp:Button ID="Button4" runat="server" class="btn btn-primary"
                        onclick="Button4_Click" Text="Buscar" />
                
                </td></tr></table>
        <br />
        <br />
        

            <table align="center"> <tr><td>
                <asp:Image ID="Image2" runat="server" />
                </td></tr></table>

 
    <div style="overflow : auto; width : 100%; max-height : 800px;">
                <%--<asp:GridView ID="GridView1" runat="server" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3"--%> 
                <asp:GridView ID="GridView1" runat="server" class="table"
            
                    EmptyDataText="No hay incidencias con esa descripcion" GridLines="Vertical" 
                    onRowCommand="GridView1_RowCommand" 
                    onselectedindexchanged="GridView1_SelectedIndexChanged1">
                    <AlternatingRowStyle BackColor="#CCFF99" />
                    <Columns>
                        <asp:ButtonField Text="Select" />
                    </Columns>
                    <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                    <%--<HeaderStyle class="bg-primary" />--%>
              <HeaderStyle BackColor="#99CC00" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EEEEEE" ForeColor="Black" font-size="10px"/>
                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#0000A9" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#000065" />
                </asp:GridView>
        </div>
           <table id="Datos" runat="server" align="center"><tr><td>
                       <asp:Label ID="Label2" runat="server" autocomplete="off" BackColor="Black" ForeColor="White">#Control*</asp:Label>
    
        </td><td>
            <asp:TextBox ID="TextBox1" runat="server" MaxLength="10" Enabled="False"></asp:TextBox>
            <asp:FilteredTextBoxExtender ID="TextBox1_FilteredTextBoxExtender" runat="server" BehaviorID="TextBox1_FilteredTextBoxExtender" FilterType="Numbers" TargetControlID="TextBox1">
            </asp:FilteredTextBoxExtender>
        </td><td>
                       <asp:Label ID="Label3" runat="server" autocomplete="off" BackColor="Black" ForeColor="White">Nombre*</asp:Label>
    
            </td><td>
                <asp:TextBox ID="TextBox2" runat="server" MaxLength="50" Enabled="False"></asp:TextBox>
                </td></tr><tr><td>
                       <asp:Label ID="Label4" runat="server" autocomplete="off" BackColor="Black" ForeColor="White">#Comprobante</asp:Label>
    
            </td><td>
            <asp:TextBox ID="TextBox10" runat="server" MaxLength="10"></asp:TextBox>
            <asp:FilteredTextBoxExtender ID="TextBox10_FilteredTextBoxExtender" runat="server" BehaviorID="TextBox10_FilteredTextBoxExtender" FilterType="Numbers" TargetControlID="TextBox10">
            </asp:FilteredTextBoxExtender>
            </td><td>
                       <asp:Label ID="Label5" runat="server" autocomplete="off" BackColor="Black" ForeColor="White">Fecha Vigencia</asp:Label>
    
            </td><td>
                <asp:TextBox ID="TextBox11" runat="server" MaxLength="12"></asp:TextBox>
                       <asp:CalendarExtender ID="TextBox11_CalendarExtender" runat="server" BehaviorID="TextBox11_CalendarExtender" TargetControlID="TextBox11">
                       </asp:CalendarExtender>
            </td></tr><tr><td>
                       <asp:Label ID="Label6" runat="server" autocomplete="off" BackColor="Black" ForeColor="White">Cantidad</asp:Label>
    
                   </td><td>
            <asp:TextBox ID="TextBox12" runat="server" MaxLength="10"></asp:TextBox>
            <asp:FilteredTextBoxExtender ID="TextBox12_FilteredTextBoxExtender" runat="server" BehaviorID="TextBox12_FilteredTextBoxExtender" FilterType="Numbers" TargetControlID="TextBox12">
            </asp:FilteredTextBoxExtender>
                   </td><td>
                       &nbsp;</td><td>
                       &nbsp;</td></tr>
        <tr>
            <td>
                       &nbsp;</td><td>
                &nbsp;</td><td>
                       &nbsp;</td><td>
                &nbsp;</td>
        </tr></table><br />
                    <div class="row">
                          <table align="center" id="botones" runat="server"><tr><td><asp:Button ID="Button2" runat="server" class="btn btn-primary"
                        onclick="Button2_Click" Text="Guardar" /></td><td>&nbsp;&nbsp;</td><td><asp:Button ID="Button3" runat="server" class="btn btn-primary"
                        onclick="Button3_Click" Text="Dar de Baja" Visible="False" /></td></tr></table>
                    </div>
    
        <asp:ToolkitScriptManager ID="ToolkitScriptManager2" runat="server">
        </asp:ToolkitScriptManager>
    
                       <asp:Label ID="Label1" runat="server" autocomplete="off" BackColor="Black" ForeColor="White"></asp:Label>
    
        <br />
   
   </div>
  
</asp:Content>
