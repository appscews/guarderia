﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Reportes.aspx.cs" Inherits="Reporte" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <div style=" margin:10px 10px 10px 10px">

    
    
    
    <div id="selectivo" runat="server" visible="true">

    </div>
    
        <div id="contenido" runat="server" visible="false">
        
        </div>
        <nav class="navbar navbar-inverse bg-inverse" >
            <div style=" float: left;text-align: center;width:100%; margin: 2px 10px;display: inline;">
                 <asp:DropDownList ID="DropDownList1" runat="server" Width="20%" ForeColor="Black" onselectedindexchanged="DropDownList1_SelectedIndexChanged" Height="20px">
                     <asp:ListItem>Asistencia</asp:ListItem>
                     <asp:ListItem>Niños</asp:ListItem>
                     <asp:ListItem>Pagos</asp:ListItem>
                 </asp:DropDownList>
            </div>
        </nav>

        <table><tr><td>
                       <asp:Label ID="Label2" runat="server" autocomplete="off" BackColor="Black" ForeColor="White">Fecha Inicial</asp:Label>
    
            </td><td>
            <asp:TextBox ID="TextBox1" runat="server" MaxLength="10"></asp:TextBox>
            
                <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server" BehaviorID="TextBox1_CalendarExtender" Format="MM/dd/yyyy" TargetControlID="TextBox1">
                </asp:CalendarExtender>
            
            </td></tr><tr><td>
                       <asp:Label ID="Label3" runat="server" autocomplete="off" BackColor="Black" ForeColor="White">Fecha Final</asp:Label>
    
            </td><td>
            <asp:TextBox ID="TextBox2" runat="server" MaxLength="10"></asp:TextBox>
            
                    <asp:CalendarExtender ID="TextBox2_CalendarExtender" runat="server" BehaviorID="TextBox2_CalendarExtender" Format="MM/dd/yyyy" TargetControlID="TextBox2">
                    </asp:CalendarExtender>
            </td></tr><tr><td>
                       <asp:Label ID="Label4" runat="server" autocomplete="off" BackColor="Black" ForeColor="White">#Control</asp:Label>
    
            </td><td>
                    <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                    <asp:FilteredTextBoxExtender ID="TextBox3_FilteredTextBoxExtender" FilterType="Numbers" runat="server" BehaviorID="TextBox3_FilteredTextBoxExtender" TargetControlID="TextBox3">
                    </asp:FilteredTextBoxExtender>
            </td><td>
                    <asp:CheckBox ID="CheckBox1" runat="server" Text="Filtrar por # Control." />
                </td></tr><tr><td><asp:Button ID="Button2" runat="server" class="btn btn-primary"
                        onclick="Button2_Click" Text="Descargar" /></td></tr></table>
        <br />
        <br />
        <br />
        <asp:GridView ID="GridView1" runat="server" BackColor="Red" 
            EmptyDataText="empty" Visible="False">
        </asp:GridView>
        <br />
    </div>
</asp:Content>

