﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Altas.aspx.cs" Inherits="_Default" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        .style3
        {width: 70px;}
        .style4{ width: 10px;}
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">

   
   
    <div class="container" >
   <br />
    <table align="center"><tr><td>
                       <asp:Label ID="Label2" BackColor="Black" ForeColor="White" runat="server">#Control*</asp:Label>
    
        </td><td>
            <asp:TextBox ID="TextBox1" runat="server" MaxLength="10" autocomplete="off" ></asp:TextBox>
            <asp:FilteredTextBoxExtender ID="TextBox1_FilteredTextBoxExtender" runat="server" BehaviorID="TextBox1_FilteredTextBoxExtender" FilterType="Numbers" TargetControlID="TextBox1">
            </asp:FilteredTextBoxExtender>
        </td><td>
                       <asp:Label ID="Label3" BackColor="Black" ForeColor="White" runat="server">Nombre*</asp:Label>
    
            </td><td>
                <asp:TextBox ID="TextBox2" runat="server" MaxLength="50" autocomplete="off"></asp:TextBox>
                </td></tr><tr><td>
                       <asp:Label ID="Label4" BackColor="Black" ForeColor="White" runat="server">#EmpleadoCEWS*</asp:Label>
    
            </td><td>
                <asp:TextBox ID="TextBox3" runat="server" autocomplete="off"></asp:TextBox>
                <asp:FilteredTextBoxExtender ID="TextBox3_FilteredTextBoxExtender" FilterType="Numbers" runat="server" BehaviorID="TextBox3_FilteredTextBoxExtender" TargetControlID="TextBox3">
                </asp:FilteredTextBoxExtender>
            </td><td>
                       <asp:Label ID="Label5" BackColor="Black" ForeColor="White" runat="server">Edad</asp:Label>
    
            </td><td>
            <asp:TextBox ID="TextBox4" autocomplete="off" runat="server" MaxLength="2"></asp:TextBox>
            <asp:FilteredTextBoxExtender  ID="TextBox4_FilteredTextBoxExtender" runat="server" BehaviorID="TextBox4_FilteredTextBoxExtender" FilterType="Numbers" TargetControlID="TextBox4">
            </asp:FilteredTextBoxExtender>
            </td></tr><tr><td>
                       <asp:Label ID="Label6" BackColor="Black" ForeColor="White" runat="server">TUTOR (CEWS)*</asp:Label>
    
            </td><td>
                <asp:TextBox autocomplete="off" ID="TextBox5" runat="server" MaxLength="50"></asp:TextBox>
            </td><td>
                       <asp:Label ID="Label7" BackColor="Black" ForeColor="White" runat="server">TELEFONO</asp:Label>
    
            </td><td>
                <asp:TextBox ID="TextBox6" autocomplete="off" runat="server"></asp:TextBox>
                <asp:MaskedEditExtender ID="TextBox6_MaskedEditExtender" runat="server" BehaviorID="TextBox6_MaskedEditExtender" Century="2000" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder="" TargetControlID="TextBox6" Mask="999-999-9999" 
 AcceptNegative="None" MessageValidatorTip="true"  MaskType="Number" ClearMaskOnLostFocus="false" ClearTextOnInvalid="false">
                </asp:MaskedEditExtender>
            </td></tr>
        <tr>
            <td>
                       <asp:Label ID="Label8" BackColor="Black" ForeColor="White" runat="server">TUTOR 2</asp:Label>
    
            </td><td>
                <asp:TextBox ID="TextBox7" autocomplete="off" runat="server" MaxLength="50"></asp:TextBox>
            </td><td>
                       <asp:Label ID="Label9" BackColor="Black" ForeColor="White" runat="server">TELEFONO</asp:Label>
    
            </td><td>
                <asp:TextBox ID="TextBox8" autocomplete="off" runat="server"></asp:TextBox>
                <asp:MaskedEditExtender ID="TextBox8_MaskedEditExtender" runat="server" BehaviorID="TextBox8_MaskedEditExtender" Century="2000" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder="" TargetControlID="TextBox8" Mask="999-999-9999" 
 AcceptNegative="None" MessageValidatorTip="true"  MaskType="Number" ClearMaskOnLostFocus="false" ClearTextOnInvalid="false">
                </asp:MaskedEditExtender>
            </td>
        </tr></table><table align="center"><tr><td><table runat="server" id="FU" align="center"><tr><td>
        <asp:FileUpload ID="FileUpload1" runat="server" ForeColor="#000099" />
            </td></tr></table></td></tr>
       </table><br />
        <table align="center"><tr><td>                          <asp:Button ID="Guardar" runat="server" class="btn btn-primary"
                        onclick="Button1_Click" Text="Guardar" />
                    </td></tr></table>
                       <asp:Label ID="Label1" runat="server" BackColor="Black" Font-Bold="True" Font-Size="Medium" ForeColor="White"></asp:Label>
    
        <asp:ToolkitScriptManager ID="ToolkitScriptManager2" runat="server">
        </asp:ToolkitScriptManager>
    
        <br />
   
   </div>
  
</asp:Content>
